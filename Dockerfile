
### DEPRECATED: Now there is an official image: https://hub.docker.com/r/sonarsource/sonar-scanner-cli
## Build stage
#FROM maven:slim as build
FROM amd64/eclipse-temurin:11.0.20.1_1-jdk as build

ARG SONAR_RUNNER_HOME=/opt/sonar-scanner

WORKDIR /opt/sonar-scanner

RUN apt-get update && \
        apt-get -y --no-install-recommends install gnupg2 git unzip maven && \
        apt-get clean && \
        mkdir -p ${SONAR_RUNNER_HOME} && \
        git clone https://github.com/SonarSource/sonar-scanner-cli.git && \
        cd sonar-scanner-cli && \
        mvn package && \
        cp target/sonar-scanner-*.zip /root/sonar-scanner.zip && \
        TMPD=$(mktemp -d) &&  \
        unzip -d ${TMPD} /root/sonar-scanner.zip && \
        mv -f  ${TMPD}/sonar-scanner-*/* ${SONAR_RUNNER_HOME}/ 


## Runnable image
FROM amd64/eclipse-temurin:11-jre

ENV SONAR_RUNNER_HOME=/opt/sonar-scanner
ENV PATH ${SONAR_RUNNER_HOME}/bin:$PATH

COPY --from=build $SONAR_RUNNER_HOME $SONAR_RUNNER_HOME

CMD sonar-scanner
